
This repository contains my own implementations of various machine learning techniques.

bayesian_optimaization
----------------------
Software implementing bayesian optimization.

Finds global minima of an unknown function defined in 'function(x)', a plot is displayed showing the true function
(black line), a rendition of the surrogate function (coloured lines) and the points the objective function was 
evaluated at (black stars).