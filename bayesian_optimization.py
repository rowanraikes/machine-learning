import numpy as np 
import matplotlib.pyplot as plt

from scipy.spatial.distance import cdist
from scipy.stats import norm


def function(x):

	# unknown function

	alpha1 = 2
	alpha2 = 1
	beta = 0

	return np.sin(3.0*x) - alpha1*x + alpha2*x**2

def evaluate(X):

	f = np.zeros(X.size)

	for i in range(X.size):

		f[i] = function(X[i])

	return f

def rbf_kernel(x1, x2, varSigma, lengthscale): 

	if x2 is None:
		d = cdist(x1.reshape(-1,1), x1.reshape(-1,1)) 
	else: 
		d = cdist(x1.reshape(-1,1), x2.reshape(-1,1))

	K = varSigma*np.exp(-np.power(d, 2)/lengthscale)

	return K

def surragate_belief(x, f, xstar):

	varSigma = 1
	lengthScale = 1

	k_starX = rbf_kernel(xstar, x, varSigma, lengthScale) 
	k_xx = rbf_kernel(x, None, varSigma, lengthScale) 
	k_starstar = rbf_kernel(xstar, None, varSigma, lengthScale)

	mu_star = k_starX.dot(np.linalg.inv(k_xx)).dot(f) 
	varSig_star = k_starstar - (k_starX).dot(np.linalg.inv(k_xx)).dot(k_starX.T)

	return mu_star, varSig_star

def expected_improvement(f_star, x, mu, var):

	if var != 0:
	
		sd = np.sqrt(var)

		cdf = norm.cdf(f_star, mu, sd)
		pdf = norm.pdf(f_star, mu, sd)

		alpha = (f_star - mu)*cdf + sd*pdf
	else:
		alpha = 0

	return alpha

N = 100

X = np.linspace(-3, 3, N)

# choose random input set, as a subset of X
index = np.random.permutation(N)
x = X[index[0:3]] # subset defined here

# evaluate function at input locations
f = np.zeros(x.size)

for i in range(x.size):

	f[i] = function(x[i])

# find minima of evalutions
f_min = np.amin(f)

x_star = np.delete(X, index[0:3])

# bayesian optimisation loop
iterations = 5

for i in range(iterations):

	# calculate surrogate function (function approximation)
	mu_star, var_star = surragate_belief(x, f, x_star)

	# evaluate aquisition function for all remaining x
	alpha_max = 0
	index = 0 # index of best new value to use
	for j in range(x_star.size):

		alpha = expected_improvement(f_min, x_star[j], mu_star[j], var_star[j,j])


		if alpha > alpha_max:
			alpha_max = alpha
			index = j


	# update sets
	x = np.append(x, x_star[index])
	f = np.append(f, function(x_star[index]))
	x_star = np.delete(x_star, index)

	# update f_min
	f_min = np.amin(f)

	min_index = np.where(f==f_min)

	minima = x[min_index]

print('Minima Found at:',minima.flatten())

# plot surrogate function
mu_star, var_star = surragate_belief(x, f, x_star)
f_star = np.random.multivariate_normal(mu_star.flatten(), var_star, 10) 

fig = plt.figure() 
ax = fig.add_subplot(111) 

ax.plot(x_star, f_star.T) 

# plot data
ax.scatter(x, f, 50, 'k', '*', zorder=1)

# plot true function
ax.plot(X, evaluate(X), 'k')

plt.show()

